enum op { OPERATOR, OPERAND};

typedef struct stack_elem {
	op type;
	int value;
	 struct stack_elem *pp;
}stack_elem_t;

typedef struct stack {
	stack_elem_t elem;
	struct stack_elem *hp;
}stack_t;
