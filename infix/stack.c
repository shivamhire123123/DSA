#include "stack.h"


void init(stack_t *s) {
	s -> hp = NULL;
}

int push(stack_t *s, op type, int value) {
	stack_elem_t *p = (stack_elem_t *)malloc(sizeof(stack_elem_t));
	if(p == NULL) {
		return ENOMEM;
	}
	p -> pp = s -> hp;
	s -> hp = p;
	p -> type = type;
	p -> value = value;
	return 0;
}

int pop(stack_t *s, op *type, int *value) {
	stack_elem_t p = (stack_elem_t *)malloc(sizeof(stack_elem_t));
	if(p == NULL) {
		return ENOMEM;
	}
	p = s -> hp;
	p -> type = *type;
	p -> value = *value;
	s -> hp = p -> pp;
	free(p);