typedef struct node {
	char data;
	struct node *pre, *nxt;
}node_t;

typedef struct list {
	node_t *h, *t;
}list_t;
void initl(list_t *l);
int insertl(list_t *l, int pos, char data);
void appendl(list_t *l, char c);
char removel(list_t *l, int pos);
char seel(list_t *l, int pos);
void printlist(list_t *l);
void destroyl(list_t *l);
