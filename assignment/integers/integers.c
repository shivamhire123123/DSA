#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include "integers.h"

#include <stdio.h>
#include <errno.h>
#include <limits.h>

void initl(list_t *l) {
	l->h = l->t = NULL;
}

/*
 * User must call isfull before calling insertl
 * pos starts from 1
 */
int insertl(list_t *l, int pos, char data) {
	node_t *tmp = (node_t *)malloc(sizeof(node_t));
	node_t *ptr = l->h;
	int i = 1;

	if(pos <= 0) {
		return EINVAL;
	}
	while(i != pos && ptr) {		//while ptr dont point to
		i++;				//node in place of which to insert
		ptr = ptr->nxt;			//new node or till ptr is null
	}					//or ptr->nxt is null
	if(i == pos) {
		if(i == 1 && !(l->h)) {		//insert at 1 and no element in list
			l->h = l->t = tmp;
			tmp->pre = NULL;
			tmp->nxt = NULL;
		}
		else if(i == 1) {		//insert at 1 and elements
			tmp->nxt = l->h;	//already exist in list
			l->h->pre = tmp;
			tmp->pre = NULL;
			l->h = tmp;
		}
		else if(!ptr) {			//insert at the end
			l->t->nxt = tmp;
			tmp->pre = l->t;
			tmp->nxt = NULL;
			l->t = tmp;
		}
		else {				//insert elsewhere
			ptr->pre->nxt = tmp;
			tmp->nxt = ptr;
			tmp->pre = ptr->pre;
			ptr->pre = tmp;
		}
		tmp->data = data;
		return 0;
	}
	else {
		return EINVAL;
	}
}
void appendl(list_t *l, char c) {
	node_t *tmp = (node_t *)malloc(sizeof(node_t));

	tmp->pre = l->t;
	if(!(l->t)) {
		l->h = tmp;
	}
	else {
		l->t->nxt = tmp;
	}
	l->t = tmp;
	tmp->nxt = NULL;
	tmp->data = c;
}

/*
 * User must call isempty before calling removel
 * pos starts from 1
 * CHAR_MIN is return if there is no element at pos
 */
char removel(list_t *l, int pos) {

	node_t *ptr = l -> h;
	int i = 1;
	char data;

	if(pos <= 0) {
		return CHAR_MIN;
	}
	while(i != pos && ptr && ptr -> nxt != NULL) {
		i++;
		ptr = ptr->nxt;
	}
	if(i == pos) {
		if(!(l->h)) {				//no element exist
			return CHAR_MIN;
		}
		else if(i == 1 && ptr->nxt == NULL) {	//only one element exist
			l->h = l->t = NULL;
		}
		else if(ptr->pre == NULL) {		//remove first element
			l->h = ptr->nxt;
			ptr->nxt->pre = NULL;
		}
		else if(ptr->nxt == NULL) {		//remove the last element
			l->t = ptr->pre;
			ptr->pre->nxt = NULL;
		}
		else {					//remove from elsewhere
			ptr->pre->nxt = ptr->nxt;
			ptr->nxt->pre = ptr->pre;
		}
		data = ptr->data;
		free(ptr);
		return data;
	}
	else {
		return CHAR_MIN;
	}
}
char seel(list_t *l, int pos) {

	int i = 1;
	node_t *ptr = l->h;
	while(i != pos && ptr && ptr->nxt != NULL) {
		i++;
		ptr = ptr->nxt;
	}

	if(i == pos) {
		return ptr->data;
	}
	return CHAR_MIN;
}


void printlist(list_t *l) {

	node_t *ptr = l->h;

	while(ptr){
		printf("%c", ptr->data);
		ptr = ptr->nxt;
	}
	printf("\n");
}
void destroyl(list_t *l) {

	node_t *ptr = l->h, *tmp;

	if(!ptr) {
		return;
	}
	while(ptr) {
		tmp = ptr;
		ptr = ptr->nxt;
		free(tmp);
	}
	l->h = l->t = NULL;
}

void initInteger(Integer_t *a) {
	initl(a);
}
void destroyInteger(Integer_t *a) {
	destroyl(a);
}
void addDigit(Integer_t *a, char c) {
	if(c >= '0' && c <= '9')
		appendl(a, c);
}
Integer_t createIntegerFromString(char *str) {

	int i = 0;
	Integer_t a;
	initInteger(&a);
	while(str[i] != '\0') {
		addDigit(&a, str[i]);
		i++;
	}
	return a;
}
Integer_t addIntegers(Integer_t a, Integer_t b) {

	Integer_t ptr;
	int i = 1, j = 1, carry = 0;
	char c, sum = 0;
	initInteger(&ptr);
	while(seel(&a, i++) != CHAR_MIN);
	while(seel(&b, j++) != CHAR_MIN);
	i -= 2;
	j -= 2;
	while(i > 0 || j > 0) {
		if((c = seel(&a, i)) != CHAR_MIN) {
			sum += c - '0';
			c = 0;
			i--;
		}
		if((c = seel(&b , j)) != CHAR_MIN) {
			sum += c - '0';
			c = 0;
			j--;
		}
		if(carry) {
			sum += 1;
			carry = 0;
		}
		if(sum > 9) {
			carry = 1;
			sum -= 10;
		}
		insertl(&ptr, 1, sum + '0');
		sum = 0;
	}
	if(carry) {
		insertl(&ptr, 1, '1');
	}
	return ptr;

}
Integer_t substractIntegers(Integer_t a, Integer_t b) {
	Integer_t ptr;
	int i = 1, j = 1;
	char c, diff = '\0', d;
	initInteger(&ptr);
	while(seel(&a, i++) != CHAR_MIN);
	while(seel(&b, j++) != CHAR_MIN);
	i -= 2;
	j -= 2;
	while(i > 0 || j > 0) {
		if((c = seel(&a, i)) != CHAR_MIN) {
			diff += c - '0';
			c = 0;
			i--;
		}
		if((c = seel(&b, j)) != CHAR_MIN) {
			j--;
			if(diff < (c - '0')) {
				if((d = seel(&a, i)) != CHAR_MIN) {
					removel(&a, i);
					d -= 1;
					insertl(&a, i, d);
					diff += 10;
				}
				else {
					destroyl(&ptr);
					diff = 0;
					insertl(&ptr, 1, diff + '0');
					break;
				}
			}
			diff -= c - '0';
			c = 0;
		}
		insertl(&ptr, 1, diff + '0');
		diff = 0;
	}
	return ptr;
}
void printInteger(Integer_t a) {
	printlist(&a);
}
