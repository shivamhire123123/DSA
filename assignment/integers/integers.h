typedef struct node {
	char data;
	struct node *pre, *nxt;
}node_t;

typedef struct list {
	node_t *h, *t;
}list_t;
void initl(list_t *l);
int insertl(list_t *l, int pos, char data);
void appendl(list_t *l, char c);
char removel(list_t *l, int pos);
char seel(list_t *l, int pos);
void printlist(list_t *l);
void destroyl(list_t *l);

typedef list_t Integer_t;

void initInteger(Integer_t *a);
void addDigit(Integer_t *a, char c);
Integer_t createIntegerFromString(char *str);
Integer_t addIntegers(Integer_t a, Integer_t b);
Integer_t substractIntegers(Integer_t a, Integer_t b);
void printInteger(Integer_t a);
void destroyInteger(Integer_t *a);
