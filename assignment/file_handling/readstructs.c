#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

typedef struct info {
	int id;
	char name[16];
	double score;
}info_t;

void atod(double *score, char a[]) {
	double fract = 0, n;
	int i = 0, sign = 0;
	*score = 0;
	if(a[i] == '-') {
		sign = -1;
		i++;
	}
	while(a[i] != '.' && a[i] != 0) {
		*score = *score * 10 + a[i] - '0';
		i++;
	}
	if(a[i] == '.') {
		i++;
	}
	n = 0.1;
	while(a[i] != 0) {
		fract = fract + (a[i] - '0') * n;
		i++;
		n /= 10;
	}
	if(fract != 1) {
		*score = *score + fract;
	}
	if(sign < 0) {
		*score = - *score;
	}
}
int main(int argc, char *argv[]) {
	info_t **p = NULL;
	double score;
	int fh = open(argv[1], O_RDONLY),
	    n = 0,
	    count = 0,
	    i = 0;
	if(fh == -1) {
		perror("");
		return errno;
	}
	atod(&score, argv[2]);
	p = (info_t **)malloc(sizeof(info_t *) * 50);
	while(i <= 50) {
		p[i] = (info_t *)malloc(sizeof(info_t));
		i++;
	}
	read(fh, &count, sizeof(int));
	while(count--) {
		read(fh, &(p[n] -> id), sizeof(int));
		read(fh, p[n] -> name, 16);
		read(fh, &(p[n] -> score), sizeof(double));
		n++;
		if(n > 50) {
			p = (info_t **)realloc(p, sizeof(info_t *) * (n / 50  + 1) * 50);
			i = n;
			while (i < (n / 50 + 1) * 50) {
				p[i] = (info_t *)malloc(sizeof(info_t));
				i++;
			}
		}
	}
	i = 0;
	while(i <= n) {
		if(p[i] -> score == score) {
			printf("%d %s %lf\n", p[i] -> id, p[i] -> name, p[i] -> score);
		}
		free(p[i]);
		i++;
	}
	free(p);
	return 0;
}
