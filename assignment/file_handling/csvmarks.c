
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

typedef struct info {
	char *name;
	char *mis;
	char *score;
}info_t;

void atod(double *score, char a[]) {
	double fract = 0, n;
	int i = 0, sign = 0;
	*score = 0;
	if(a[i] == '-') {
		sign = -1;
		i++;
	}
	while(a[i] != '.' && a[i] != 0) {
		*score = *score * 10 + a[i] - '0';
		i++;
	}
	if(a[i] == '.') {
		i++;
	}
	n = 0.1;
	while(a[i] != 0) {
		fract = fract + (a[i] - '0') * n;
		i++;
		n /= 10;
	}
	if(fract != 1) {
		*score = *score + fract;
	}
	if(sign < 0) {
		*score = - *score;
	}
}
int ifequal(char *s1, char *s2) {
	int i = 0;
	do {
		if(s1[i] != s2[i]) {
			return 1;
		}
		i++;
	}while(s1[i] != '\0' || s2[i] != '\0');
	return 0;
}
int main(int argc, char *argv[]) {
	int i, num, fd;
	char c[1000];
	info_t data;
	fd = open(argv[1], O_RDONLY);
	do {
		i = 0;
		do {
			num = read(fd, &c[i], sizeof(char));
		}while(c[i++] != '\n' && num);
		if(!num) {
			break;
		}
		c[i++] = '\0';
		data.name = strtok(c, ", ");
		data.mis = strtok(NULL, ", ");
		data.score = strtok(NULL, ", ");
		if(data.score == NULL) {
			continue;
		}
		data.score[strlen(data.score) - 1] = '\0';
		if(!ifequal(data.score, argv[2])) {
			printf("%s\n", data.name);
		}
	}while(num);
	close(fd);
	return 0;
}
