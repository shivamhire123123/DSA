#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

typedef struct info {
	int id;
	char name[16];
	double score;
}info_t;

int main(int argc, char *argv[]) {
	info_t **p = NULL;
	int fh = open(argv[1], O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU),
	    n = 0,
	    i = 0;
	if(fh == -1) {
		perror("");
		return errno;
	}
	p = (info_t **)malloc(sizeof(info_t *) * 50);
	while(i < 50) {
		p[i] = (info_t *)malloc(sizeof(info_t));
		i++;
	}
	while(scanf("%d%s%lf", &(p[n] -> id), p[n] -> name, &(p[n] -> score)) != EOF) {
		n++;
		if(n >= 50) {
			p = (info_t **)realloc(p, sizeof(info_t *) * (n / 50  + 1) * 50);
			i = n;
			while (i < (n / 50 + 1) * 50) {
				p[i] = (info_t *)malloc(sizeof(info_t));
				i++;
			}
		}
	}
	write(fh, &n, sizeof(int));
	i = 0;
	while(i < n) {
		write(fh, &(p[i] -> id), sizeof(int));
		write(fh, p[i] -> name, 16);
		write(fh, &(p[i] -> score), sizeof(double));
		i++;
	}
	while(n--){
		free(p[n]);
	}
	free(p);
	close(fh);
	return 0;
}
