
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

typedef struct data {
	int age;
	char name[16];
}data_t;

int main(int argc, char *argv[]) {
	int n, i, num, m, j;
	char c;
	data_t p;
	int fh = open(argv[1], O_RDONLY);
	if(fh == -1) {
		perror("");
		return errno;
	}
	read(fh, &n, sizeof(int));
	i = 0;
	while(i < n) {
		read(fh, &num, sizeof(int));
		printf("%d\n", num);
		i++;
	}
	read(fh, &m, sizeof(int));
	i = 0;
	while(i < m) {
		read(fh, &(p.age), sizeof(int));
		read(fh, p.name, 16);
		printf("%d %s\n", p.age, p.name);
		i++;
	}
	read(fh, &j, sizeof(int));
	i = 0;
	while(i < j) {
		read(fh, &c, sizeof(char));
		while(c != 0){
			printf("%c", c);
			read(fh, &c, sizeof(char));
		}
		printf("\n");
		i++;
	}
	close(fh);
	return 0;

}
