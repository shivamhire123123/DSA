#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


int main(int argc, char *argv[]) {
	int fd, i = 0;
	fd = open("./decodeme", O_RDONLY);
	long mis;
	char name[40];
	if(fd == -1) {
		perror("");
		return errno;
	}
	while(i <= 5) {
		read(fd, &mis, sizeof(long));
		read(fd, name, 40);
		printf("%ld%s\n", mis, name);
		i++;
	}
	close(fd);
	return 0;
}
