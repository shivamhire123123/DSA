typedef struct node {
	char* data;
	struct node *nxt;
}node_t;

typedef struct list {
	node_t *h, *t;
}list_t;
void init(list_t *l);
void printl(list_t *l);
int insert(list_t *l, char *str, int pos);
char *remov(list_t *l, int pos);
void sort(list_t *l);
void reverse(list_t *l);
void destroy(list_t *l);
