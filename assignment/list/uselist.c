#include "list.h"
#include <stdlib.h>
#include <stdio.h>

int main() {

	list_t l;
	char choice, str[100], *c;
	int pos, exit = 0;
	init(&l);
	while(1) {
		scanf("%c", &choice);
		getchar();
		switch(choice) {
			case 'i':
				scanf("%s %d", str, &pos);
				getchar();
				insert(&l, str, pos);
				break;
			case 'd':
				scanf("%d", &pos);
				getchar();
				c = remov(&l, pos);
				if(c) {
					printf("%s\n", c);
					free(c);
				}
				break;
			case 's':
				sort(&l);
				break;
			case 'r':
				reverse(&l);
				break;
			default:
				destroy(&l);
				exit = 1;
				break;
		}
		if(exit)
			break;
		else {
			printl(&l);
		}

	}
	return 0;
}
