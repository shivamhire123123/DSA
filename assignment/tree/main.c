#include "tree.h"

int main(int argc, char *argv[]) {
	tree t;
	if(argc != 3) {
		printf("Invalid arguments\n");
		return EINVAL;
	}
	FILE *readfrom = fopen(argv[1], "r");
	FILE *writeto = fopen(argv[2], "w");
	tinit(&t);
	data_t d;
	while(fscanf(readfrom, "%s", d.name) != -1) {
		fscanf(readfrom, "%d", &d.marks);
		tinsert(&t, d);
		fseek(readfrom, 1, SEEK_CUR);
	}
	csvinorder(t, writeto);
	return 0;
}
