#include "tree.h"

void tinit(tree *t) {
	*t = NULL;
}

void tinsert(tree *t, data_t data) {
	node_t *p, *q;
	if(!(*t)) {
		*t = p = (node_t *)malloc(sizeof(node_t));
		p->d = data;
		p->left = p->right = NULL;
		return;
	}
	p = *t;
	while(p) {
		q = p;
		if(strcmp(p->d.name, data.name) > 0) {
			p = p->left;
		}
		else if(strcmp(p->d.name, data.name) < 0) {
			p = p->right;
		}
		else
			return;
	}
	if(strcmp(q->d.name, data.name) > 0) {
		q->left = (node_t *)malloc(sizeof(node_t));
		q = q->left;
	}
	else {
		q->right = (node_t *)malloc(sizeof(node_t));
		q = q->right;
	}
	q->d = data;
	q->left = q->right = NULL;
}

void csvinorder(node_t *t, FILE *writeto) {
	if(!t) {
		return;
	}
	csvinorder(t->left, writeto);
	fprintf(writeto, "%s %d\n", t->d.name, t->d.marks);
	csvinorder(t->right, writeto);
}
