#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

typedef struct data {
	char name[16];
	int marks;
}data_t;

typedef struct node {
	data_t d;
	struct node *left, *right;
}node_t;

typedef node_t * tree;

void tinit(tree *t);

void tinsert(tree *t, data_t data);

void csvinorder(node_t *t, FILE *writeto);
