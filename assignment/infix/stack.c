#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
void init(stack_t *s) {
	s -> s = NULL;
	s -> buff = NULL;
}
void push(stack_t *s, int no) {
	node_t *tmp;
	if(s -> buff == NULL) {
		tmp = (node_t *)malloc(sizeof(node_t));
	}
	else {
		tmp = s -> buff;
		s -> buff = NULL;
	}
	tmp->val = no;
	tmp->next = s -> s;
	s -> s = tmp;
}
int pop(stack_t *s) {
	node_t *tmp = s -> s;
	int val = s -> s -> val;
	(s -> s) = (s -> s)->next;
	free(tmp);
	if(s -> buff) {
		free(s -> buff);
		s -> buff = NULL;
	}
	return val;
}
int top(stack_t *s) {
	int x = s -> s -> val;
	return x;
}
int isempty(stack_t *s) {
	return (s -> s) == NULL;
}
int isfull(stack_t *s) {
	if((s -> buff) == NULL) {
		s -> buff = (node_t *) malloc(sizeof(node_t));
	}
	return (s -> buff) == NULL;
}
void print(stack_t *s) {
	node_t *tmp = s -> s;
	while(tmp != NULL) {
		printf("%d ", tmp -> val);
		tmp = tmp -> next;
	}
	printf("\n");
}
void frees(stack_t *s) {
	while(!isempty(s)) {
		pop(s);
	}
}
