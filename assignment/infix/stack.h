typedef struct node {
	int val;
	struct node *next;
}node_t;
typedef struct stack {
	node_t *s;
	node_t *buff;
}stack_t;
void init(stack_t *s);
void push(stack_t *s, int no);
int pop(stack_t *s);
int top(stack_t *s);
int isempty(stack_t *s);
int isfull(stack_t *s);
void print(stack_t *s);
void frees(stack_t *s);
