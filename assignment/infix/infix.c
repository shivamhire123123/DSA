#include <stdio.h>
#include <limits.h>
#include "stack.h"

enum state {SPC, OPR, NUM, BRC, END, ERR};
typedef struct {
	int type;
	int val;
}token_t;

token_t gettoken(char *s, int *reset) {

	token_t t;
	char curchar;
	static int i = 0;
	static enum state curstate = SPC;
	static int no = 0;
	if(*reset == 1) {
		*reset = 0;
		curstate = SPC;
		i = 0;
	}

	while(1) {
		curchar = s[i];
		switch(curstate) {
			case SPC:
				switch(curchar) {
					case ' ':
						break;
					case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': 
					case '9': case '0':
						no = curchar - '0';
						curstate = NUM;
						break;
					case '+': case '-': case '*': case '/':
					case '%':
						curstate = OPR;
						break;
					case '\0':
						t.type = END;
						return t;
					case '(': case ')':
						curstate = BRC;
						break;
					default:
						t.type = ERR;
						return t;
				}
				i++;
				break;
			case NUM:
				switch(curchar) {
					case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': 
					case '9': case '0':
						no = no * 10 + curchar - '0';
						i++;
						break;
					case ' ':
						curstate = SPC;
						t.type = NUM;
						t.val = no;
						i++;
						return t;
						break;
					case '+': case '-': case '*': case '/':
					case '%':
						curstate = OPR;
						t.type = NUM;
						t.val = no;
						i++;
						return t;
						break;
					case '\0':
						curstate = END;
						t.type = NUM;
						t.val = no;
						return t;
						break;
					case '(': case ')':
						curstate = BRC;
						t.type = NUM;
						t.val = no;
						i++;
						return t;
						break;
					default:
						t.type = ERR;
						return t;
				}
				break;
			case OPR:
				t.type = OPR;
				t.val = s[i - 1];
				i++;
				switch(curchar) {
					case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': 
					case '9': case '0':
						curstate = NUM;
						no = curchar - '0';
						break;
					case ' ':
						curstate = SPC;
						break;
					case '+': case '-': case '*': case '/':
					case '%':
						break;
					case '\0':
						curstate = END;
						break;
					case '(': case ')':
						curstate = BRC;
						break;
					default:
						curstate = ERR;
						break;
				}
				return t;
				break;
			case END:
				t.type = END;
				return t;
				break;
			case BRC:
				t.type = BRC;
				t.val = s[i - 1];
				switch(curchar) {
					case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': 
					case '9': case '0':
						no = curchar - '0';
						curstate = NUM;
						i++;
						break;
					case ' ':
						curstate = SPC;
						i++;
						break;
					case '+': case '-': case '*': case '/':
					case '%':
						curstate = OPR;
						i++;
						break;
					case '\0':
						curstate = END;
						break;
					case '(': case ')':
						i++;
						break;
				}
				return t;
			case ERR:
				t.type = ERR;
				return t;
				break;
		}
	}
}
int precedence(int x) {
	switch(x) {
		case '+': case '-':
			return 0;
		case '*': case '/':
			return 1;
		case '%':
			return 2;
		default:
			return 3;
	}
}
int infix(char *expr) {
	token_t t;
	stack_t opd_stack, opr_stack;
	init(&opd_stack);
	init(&opr_stack);
	int pre_opr, opd2, opd1, result;
	int reset = 1, brc = 0;
	while(1) {
		t = gettoken(expr, &reset);
		if(t.type == NUM) {
			if(!isfull(&opd_stack)) {
				push(&opd_stack, t.val);
			}
			else {
				fprintf(stderr, "Memory full\n");
				frees(&opd_stack);
				frees(&opr_stack);
				return INT_MAX;
			}
		}
		else if(t.type == BRC) {
			if(t.val == '(') {
				if(!isfull(&opr_stack)) {
					brc++;
					push(&opr_stack, t.val);
				}
				else {
					fprintf(stderr, "Memory full\n");
				}
			}
			else {
				while(!isempty(&opr_stack) && brc > 0) {
					pre_opr = pop(&opr_stack);
					if(pre_opr != '(') {
						if(!isempty(&opd_stack)) {
							opd2 = pop(&opd_stack);
						}
						else {
							fprintf(stderr, "Less operand\n");
							return INT_MAX;
						}
						if(!isempty(&opd_stack)) {
							opd1 = pop(&opd_stack);
						}
						else {
							fprintf(stderr, "Less operand\n");
							frees(&opd_stack);
							frees(&opr_stack);
							return INT_MAX;
						}
						switch(pre_opr) {
							case '+':
								result = opd2 + opd1;
								break;
							case '-':
								result = opd1 - opd2;
								break;
							case '*':
								result = opd1 * opd2;
								break;
							case '/':
								if(opd2 == 0) {
									fprintf(stderr, "Divide by 0\n");
									frees(&opd_stack);
									frees(&opr_stack);
									return INT_MAX;
								}
								result = opd1 / opd2;
								break;
							case '%':
								result = opd1 % opd2;
								break;
							default :
								frees(&opd_stack);
								frees(&opr_stack);
								return INT_MAX;
						}
						push(&opd_stack, result);
					}
					else {
						brc--;
						break;
					}
				}
			}
		}
		else if(t.type == OPR) {
			if(!isempty(&opr_stack)) {
				pre_opr = top(&opr_stack);
				while(pre_opr != '(' && precedence(pre_opr) >= precedence(t.val)) {
					if(!isempty(&opd_stack)) {
						opd2 = pop(&opd_stack);
					}
					else {
						fprintf(stderr, "Less operand\n");
						frees(&opd_stack);
						frees(&opr_stack);
						return INT_MAX;
					}
					if(!isempty(&opd_stack)) {
						opd1 = pop(&opd_stack);
					}
					else {
						fprintf(stderr, "Less operand\n");
						frees(&opd_stack);
						frees(&opr_stack);
						return INT_MAX;
					}
					switch(pre_opr) {
						case '+':
							result = opd1 + opd2;
							break;
						case '-':
							result = opd1 - opd2;
							break;
						case '*':
							result = opd1 * opd2;
							break;
						case '/':
							if(opd2 == 0) {
								fprintf(stderr, "Divide by 0\n");
								frees(&opd_stack);
								frees(&opr_stack);
								return INT_MAX;
							}
							result = opd1 / opd2;
							break;
						case '%':
							result = opd1 % opd2;
							break;
						default :
							frees(&opd_stack);
							frees(&opr_stack);
							return INT_MAX;
					}
					push(&opd_stack, result);
					pop(&opr_stack);
					if(isempty(&opr_stack)) {
						break;
					}
					pre_opr = top(&opr_stack);
				}
			}
			push(&opr_stack, t.val);
		}
		else if(t.type == ERR) {
			frees(&opd_stack);
			frees(&opr_stack);
			return INT_MAX;
		}
		else if(t.type == END) {
			while(!isempty(&opr_stack)) {
					pre_opr = pop(&opr_stack);
					if(!isempty(&opd_stack)) {
						opd2 = pop(&opd_stack);
					}
					else {
						fprintf(stderr, "Less operand\n");
						frees(&opd_stack);
						frees(&opr_stack);
						return INT_MAX;
					}
					if(!isempty(&opd_stack)) {
						opd1 = pop(&opd_stack);
					}
					else {
						fprintf(stderr, "Less operand\n");
						frees(&opd_stack);
						frees(&opr_stack);
						return INT_MAX;
					}
					switch(pre_opr) {
						case '+':
							result = opd2 + opd1;
							break;
						case '-':
							result = opd1 - opd2;
							break;
						case '*':
							result = opd1 * opd2;
							break;
						case '/':
							if(opd2 == 0) {
								fprintf(stderr, "Divide by 0\n");
								return INT_MAX;
							}
							result = opd1 / opd2;
							break;
						case '%':
							result = opd1 % opd2;
							break;
						default :
							frees(&opd_stack);
							frees(&opr_stack);
							return INT_MAX;
					}
					push(&opd_stack, result);
			}
			result = pop(&opd_stack);
			if(!isempty(&opd_stack)) {
				fprintf(stderr, "Less operator\n");
				frees(&opd_stack);
				frees(&opr_stack);
				return INT_MAX;
			}
			frees(&opd_stack);
			frees(&opr_stack);
			return result;
		}
		else {
			frees(&opd_stack);
			frees(&opr_stack);
			return INT_MAX;
		}
	}
}



int readline(char *line, int len) {
	int i = 0;
	char c;
	while(i < len && (c = getchar()) != -1 && c != '\n') {
		line[i++] = c;
	}
	line[i] = '\0';
	return i;
}
int main() {
	char expr[128];
	int r;
	while(readline(expr, 128)) {
		r = infix(expr);
		if(r == INT_MAX) {
			fprintf(stderr, "Wrong Expression\n");
		}
		else {
			printf("%d\n", r);
		}
	}
	return 0;
}
