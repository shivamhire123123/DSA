#include "queue.h"
void qinit(queue_t *q) {
	q->h = q->t = NULL;
}
void printq(queue_t *q) {
	node_t *ptr = q->h;
	if(ptr) {
		do {
			printf("%s %d ", ptr->data.name, ptr->data.age);
		} while(ptr != q->t && (ptr = ptr->nxt));
	}
	printf("\n");
}
/*user must call qfull first to see if memory is available*/
void enq(queue_t *q, data_t data) {
	node_t *ptr = q->h;
	node_t *tmp;
	if(q->buf) {
		tmp = q->buf;
		q->buf = NULL;
	}
	else {
		tmp = (node_t *)malloc(sizeof(node_t));
	}
	strcpy(tmp->data.name, data.name);
	tmp->data.age = data.age;
	/*how many nodes are present*/
	/*no node*/
	if(!ptr) {
		q->h = q->t = tmp;
	}
	/*one node*/
	else if(ptr == q->t) {
		ptr->nxt = q->t = tmp;
	}
	/*more than one node*/
	else if(ptr->nxt != NULL) {
		while(ptr->nxt != q->t) {
			ptr = ptr->nxt;
		}
		ptr->nxt->nxt = tmp;
		q->t = tmp;
	}
	tmp->nxt = q->h;
}
/*user must call qempty first to see if queue is empty*/
data_t deq(queue_t *q) {
	node_t *tmp = q->h;
	data_t data;
	strcpy(data.name, tmp->data.name);
	data.age = tmp->data.age;
	if(tmp->nxt != tmp) {
		q->h = tmp->nxt;
	}
	else {
		q->h = NULL;
	}
	free(tmp);
	if(q->h) {
		q->t->nxt = q->h;
	}
	else {
		q->t = NULL;
	}
	if(q->buf) {
		free(q->buf);
		q->buf = NULL;
	}
	return data;
}
int qfull(queue_t *q) {

	if(q->buf) {
		return 0;
	}
	else {
		q->buf = (node_t *)malloc(sizeof(node_t));
		if(!(q->buf)) {
			return 1;
		}
		else {
			return 0;
		}
	}
}
int qempty(queue_t *q) {
	return q->h == NULL;
}
