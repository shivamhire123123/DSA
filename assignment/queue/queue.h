#include <stdlib.h>
#include <stdio.h>
#include <string.h>
typedef struct data {
	char name[16];
	unsigned int age;
}data_t;

typedef struct node {
	data_t data;
	struct node *nxt;
}node_t;

typedef struct queue {
	node_t *h, *t, *buf;
}queue_t;

void qinit(queue_t *q);
void printq(queue_t *q);
/*user must call qfull first to see if memory is available*/
void enq(queue_t *q, data_t data);
/*user must call qempty first to see if queue is empty*/
data_t deq(queue_t *q);
int qfull(queue_t *q);
int qempty(queue_t *q);
