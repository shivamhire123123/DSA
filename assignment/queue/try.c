#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

int main() {

	queue_t l;
	char choice;
	data_t data;
	int pos, exit = 0;
	qinit(&l);
	while(1) {
		scanf("%c", &choice);
		getchar();
		switch(choice) {
			case 'i':
				scanf("%s %d", data.name, &data.age);
				getchar();
				if(!qfull(&l)) {
					enq(&l, data);
				}
				else {
					printf("Mem full\n");
				}
				break;
			case 'd':
				if(!qempty(&l)) {
					data = deq(&l);
					printf("%s %d\n", data.name, data.age);
				}
				else {
					printf("q empty\n");
				}
				break;
			case 's':
//				sort(&l);
				break;
			case 'r':
//				reverse(&l);
				break;
			default:
//				destroy(&l);
				exit = 1;
				break;
		}
		if(exit)
			break;
		else {
			printq(&l);
		}

	}
	return 0;
}
