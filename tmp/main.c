#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>

int main() {
	char filename[128];
	scanf("%s", filename);
	int fd = open(filename, O_EXCL | O_CREAT, S_IRWXU);
	if(fd == -1) {
		perror("");
		return errno;
	}
	FILE *file = fdopen(fd, "w+");
	fprintf(file, "hello");
}

