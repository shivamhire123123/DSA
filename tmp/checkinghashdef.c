#include <stdio.h>
#include "../src/piece/piece.h"
#include "../src/includes.h"
#include <stdlib.h>
#include "../src/debug.h"

void printpieceinfo(piece_t *piece) {
	vprint2("piece -> %c", piece->piece);
	vprint2("color -> %lx", COLOR(piece->bitpiece));
	vprint2("Vert -> %lx, Pve -> %lx, Nve -> %lx", GetV(piece->bitpiece),
												GetVPveField(piece->bitpiece),
												GetVNveField(piece->bitpiece));
	vprint2("Hori -> %lx, Pve -> %lx, Nve -> %lx", GetH(piece->bitpiece),
												GetHPveField(piece->bitpiece),
												GetHNveField(piece->bitpiece));
	vprint2("Diagonal -> %lx, NW -> %lx, NE -> %lx, SW -> %lx, SE -> %lx",
												GetD(piece->bitpiece),
												GetDNWField(piece->bitpiece),
												GetDNEField(piece->bitpiece),
												GetDSWField(piece->bitpiece),
												GetDSEField(piece->bitpiece));
	vprint2("TwoMoves : NW -> %lx, NE -> %lx, SW -> %lx, SE -> %lx, WN-> %lx, WS -> %lx, EN -> %lx, ES -> %lx",
												GetNightNW(piece->bitpiece),
												GetNightNE(piece->bitpiece),
												GetNightSW(piece->bitpiece),
												GetNightSE(piece->bitpiece),
												GetNightWN(piece->bitpiece),
												GetNightWS(piece->bitpiece),
												GetNightEN(piece->bitpiece),
												GetNightES(piece->bitpiece));
}
int main() {
	piece_t *piece;
	piece = (piece_t *)malloc(sizeof(piece_t));
	scanf("%lx", &piece->bitpiece);
//	SetDNEField(piece, 1);
//	SetNightNE(piece, 1);
//	printf("%lx\n", piece->bitpiece);
	printpieceinfo(piece);
}
