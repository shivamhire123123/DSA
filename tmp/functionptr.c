#include <stdio.h>

int f1() {
	printf("In f1\n");
	return 0;
}
void f2() {
	printf("In f2\n");
}
void f3() {
	printf("In f3\n");
}
int main() {
	int (* func)(void) = f1;
	return 0;

}
