#include <stdio.h>
#include <errno.h>
unsigned long long fact(int n) {
	unsigned long long pro = n;
	int i = 2;
	if(n < 0) {
		return EPERM;
	}
	while(i < n) {
		pro *= i;
		i++;
	}
	return pro;
}
int main() {
	unsigned long long n;
//	scanf("%llu", &n);
	n = 20;
	if(n > 0)
		printf("%llu\n", fact(n));
	else
		return EPERM;
	return 0;
}
