#include <stdio.h>
typedef unsigned long long ull;
ull pow(int x, int y) {
	ull pro = 1;
	while(y > 0) {
		y >>= 1;
		x *= x;
		if(y & 1) {
			pro *= x;
		}
	}
	return pro;
}
int main() {
	int x = 10, y = 10;
//	scanf("%d%d", &x, &y);
	printf("%llu\n", pow(x, y));
	return 0;
}
