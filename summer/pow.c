#include <stdio.h>
typedef unsigned long long ull;
ull pow(int x, int y){
	ull pro = x;
	int i = 1;
	while(i < y) {
		pro *= x;
		i++;
	}
	return pro;
}
int main() {
	int x = 10, y = 10;
	printf("%llu\n",pow(x, y));
	return 0;
}
