#include "stack.h"

void init(stack_t *stack) {
	stack -> i = 0;
}

void push(stack_t *stack, pos_t pos) {
	stack -> pos[stack -> i].x = pos.x;
	stack -> pos[stack -> i++].y = pos.y;
}

pos_t pop(stack_t *stack) {
	return stack -> pos[stack -> i--];
}

int isempty(stack_t *stack) {
	return !(stack -> i);
}

int isfull(stack_t *stack) {
	return stack -> i == 4;
}

static void makeequal(stack_t *stack1, stack_t *stack2) {
	int i;
	for(i = 0; i < 4; i++) {
		stack1 -> pos[i] = stack2 -> pos[i];
	}
	stack1 -> i = stack2 -> i;
}

void rsinit(rstack_t *rstack) {
	int i = 0;
	rstack -> i = MAX - 1;
	for(i = 0; i < MAX; i++) {
		init(&(rstack -> surr_stack[i].stack));
	}
}

void rspush(rstack_t *rstack, stack_t stack, pos_t prev_pos) {
	makeequal(&rstack -> surr_stack[rstack -> i].stack, &stack);
	rstack -> surr_stack[rstack -> i--].prev_pos = prev_pos;
}

void rspop(rstack_t *rstack, stack_t *stack, pos_t *prev_pos) {
	makeequal(stack, &rstack -> surr_stack[rstack -> i].stack);
	*prev_pos = rstack -> surr_stack[rstack -> i++].prev_pos;
}

int rsisempty(rstack_t *rstack) {
	return rstack -> i == MAX - 1;
}

int rsisfull(rstack_t *rstack) {
	return rstack -> i == -1;
}
