#define MAX 100

typedef struct pos {
	int x;
	int y;
}pos_t;

typedef struct stack {
	pos_t pos[4];
	int i;
}stack_t;

void init(stack_t *stack);
void push(stack_t *stack, pos_t pos);
pos_t pop(stack_t *stack);
int isempty(stack_t *stack);
int isfull(stack_t *stack);

typedef struct surr_stack {
	stack_t stack;
	pos_t prev_pos;
}surr_stack_t;

typedef struct rstack {
	surr_stack_t surr_stack[MAX];
	int i;
}rstack_t;

void rsinit(rstack_t *rstack);
void rspush(rstack_t *rstack, stack_t stack, pos_t pos);
void rspop(rstack_t *rstack, stack_t *stack, pos_t *pos);
int rsisempty(rstack_t *rstack);
int rsisfull(rstack_t *rstack);
