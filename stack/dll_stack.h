/*
 * integer stack as a double link null terminated structure
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct dll_node {
	int data;
	struct dll_node  *pre, *nxt;
}dll_node_t;

typedef struct stack {
	dll_node_t *h, *t;
	dll_node_t *buff;
}dll_stack_t;

/*
 * This function must be called after making a dll_stack_t type variable and before
 * performing any other action on it
 */
void dll_init(dll_stack_t *s);
/*
 * Push a 'no' onto the stack
 */
void dll_push(dll_stack_t *s, int data);
/*
 * Pop an integer from the stack
 */
int dll_pop(dll_stack_t *s);
/*
 * to see what is the top most number on the stack
 */
int dll_top(dll_stack_t *s);
/*
 * This fuction must be called before poping any integer from stack to see if
 * stack is not empty
 */
int dll_isempty(dll_stack_t *s);
/*
 * This fuction must be called before pushing any integer to the stack to see if
 * stack is not full
 */
int dll_isfull(dll_stack_t *s);
/*
 * To print all the elements of the stack.
 * All stack elements is still retain on stack
 */
void dll_print(dll_stack_t *s, FILE *stream);
/*
 * This fuction must be called before ending the program to delete all malloc
 * memory
 */
void dll_freestack(dll_stack_t *s);
