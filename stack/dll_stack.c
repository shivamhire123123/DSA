/*
 * integer stack as a double link null terminated structure
 */
#include "dll_stack.h"
/*
 * This function must be called after making a dll_stack_t type variable and before
 * performing any other action on it
 */
void init(dll_stack_t *s) {
	s -> h = s -> t = s -> buff = NULL;
}

/*
 * This fuction must be called before poping any integer from stack to see if
 * stack is not empty
 */
int isempty(dll_stack_t *s) {
	return s -> h == NULL;
}

/*
 * This fuction must be called before pushing any integer to the stack to see if
 * stack is not full
 */
int isfull(dll_stack_t *s) {
	if((s -> buff) == NULL) {
		s -> buff = (dll_node_t *) malloc(sizeof(dll_node_t));
	}
	return (s -> buff) == NULL;
}


/*
 * Push a 'no' onto the stack
 */
void push(dll_stack_t *s, int no) {
	dll_node_t *tmp;
	if(s -> buff == NULL) {
		tmp = (dll_node_t *)malloc(sizeof(dll_node_t));
	}
	else {
		tmp = s -> buff;
		s -> buff = NULL;
	}
	tmp -> data = no;
	if(s -> h) {
		s -> h -> pre = tmp;
	}
	tmp -> nxt = s -> h;
	s -> h = tmp;
	tmp -> pre = NULL;
}
/*
 * Pop an integer from the stack
 */
int pop(dll_stack_t *s) {
	dll_node_t *tmp = s -> h;
	int val = s -> h -> data;
	(s -> h) = (s -> h)-> nxt;
	s -> h -> pre = NULL;
	free(tmp);
	if(s -> buff) {
		free(s -> buff);
		s -> buff = NULL;
	}
	return val;
}
/*
 * to see what is the top most number on the stack
 */
int top(dll_stack_t *s) {
	int x = s -> h -> data;
	return x;
}
void print(dll_stack_t *s) {
	dll_node_t *tmp = s -> h;
	while(tmp != NULL) {
		printf("%d ", tmp -> data);
		tmp = tmp -> nxt;
	}
	printf("\n");
}
void frees(dll_stack_t *s) {
	while(!isempty(s)) {
		pop(s);
	}
}
