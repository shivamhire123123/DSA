#include <stdio.h>
#include <math.h>

double mycos(double rad, double precision) {

	long long mod;

	mod = rad / (2 * M_PI);
	rad = rad - mod * 2 * M_PI;

	double result = 1, t = rad * rad, factor = 1;
	double x = t, fct = 2;
	int sign = -1, i = 3;

	while(factor > precision) {
		factor = x / fct;
		result += sign * factor;
		sign = -sign;
		x *= t;
		fct *= i * (i + 1);
		i += 2;
	}

	return result;
}

int main() {

	double rad;

	scanf("%lf", &rad);
	printf("%f\n", mycos(rad, 1e-6));
	printf("%f\n", cos(rad));

	return 0;
}
