#include <stdio.h>
#include <errno.h>
//calculate x^y using step of 3
//x - base
//y - exponent
double pow3(int x, int y) {
	double res = 1;			//to store result of multiplication
					//initialise to 1 to cover x^0 case
	int sign = 0;			//to store sign of y
	if(y < 0) {			//make y positive
		sign = -1;
		y = -y;
	}
	while(y) {			//till y get 0
		if(y % 3 == 1) {
			res *= x;
		} else if(y % 3 == 2) {
			res *= x * x;
		}
		y /= 3;			//divide y by 3
		x *= x * x;		//increase x power by multiple of 3
					//i.e. 1, 3, 9, 27, ...
	}
	if(sign) {			//if y was -ve
		res = 1/res;		//reciprocate res
	}
	return res;
}
int main() {
	int x = 0, y = 0;
	if(!scanf("%d%d", &x, &y)) {
		return EINVAL;
	}
	printf("%lf\n", pow3(x, y));
}
