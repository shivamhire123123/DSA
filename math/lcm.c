#include <stdio.h>
int main() {
	/* 
	 * num1 and num2 are numbers whoes lcm is to be found
	 * i is index for for loop
	 */
	int num1, num2, i;
	scanf("%d", &num1);
	scanf("%d", &num2);
	/*
	 * to find lcm iterate i from 2 till (num1 * num2) till
	 * we find number which is divisible by both num1 and num2
	 */
	for(i = 2; i <= num1 * num2; i++) {
		if(!(i % num1) && !(i % num2)) {
			printf("%d\n", i);
			break;
		}
	}
	return 0;
}
