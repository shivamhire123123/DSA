#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
int main() {
	int fd, i = 42;
	fd = open("./file.txt", O_RDWR);
	if(fd == -1) {
		perror("");
		return errno;
	}
	write(fd, &i, sizeof(int));
}
