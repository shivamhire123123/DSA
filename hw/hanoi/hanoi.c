#include <stdio.h>
#include "stack.h"

typedef struct hanoi {
	stack_t arr[3];
}hanoi_t;

void solve(hanoi_t *hanoi, int w) {
	int i, index;
	if(!isempty(&(hanoi->arr[w]))) {
		i = pop(&(hanoi->arr[w]));
		if(i % 2 == 1) {
			index = (w + 1) % 3;
		}
		else {
			index = (w + 2) % 3;
		}
		push(&(hanoi->arr[index]), i);
		printf("%d:%d->%d\n", i, w, index);
		solve(hanoi, );
		solve(hanoi, );
	}
	return;
}

int main() {
	int i;
	hanoi_t hanoi;
	for(i = 0; i < 3; i++) {
		init(&(hanoi.arr[i]));
	}
	scanf("%d", &i);
	while(i) {
		push(&(hanoi.arr[0]), i--);
	}
	solve(&hanoi, 0);
	return 0;
}
