#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
void init(stack_t *s) {
	s -> s = NULL;
	s -> buff = NULL;
}
void push(stack_t *s, pos_t pos) {
	node_t *tmp;
	if(s -> buff == NULL) {
		tmp = (node_t *)malloc(sizeof(node_t));
	}
	else {
		tmp = s -> buff;
		s -> buff = NULL;
	}
	tmp->pos = pos;
	tmp->next = s -> s;
	s -> s = tmp;
}
pos_t pop(stack_t *s) {
	node_t *tmp = s -> s;
	pos_t pos = tmp->pos;
	(s -> s) = (s -> s)->next;
	free(tmp);
	if(s -> buff) {
		free(s -> buff);
		s -> buff = NULL;
	}
	return pos;
}
pos_t top(stack_t *s) {
	return s->s->pos;
}
int isempty(stack_t *s) {
	return (s -> s) == NULL;
}
int isfull(stack_t *s) {
	if((s -> buff) == NULL) {
		s -> buff = (node_t *) malloc(sizeof(node_t));
	}
	return (s -> buff) == NULL;
}
void print(stack_t *s) {
	node_t *tmp = s -> s;
	while(tmp != NULL) {
		printf("(%d, %d) ", tmp -> pos.row, tmp->pos.col);
		tmp = tmp -> next;
	}
	printf("\n");
}
void frees(stack_t *s) {
	while(!isempty(s)) {
		pop(s);
	}
}
