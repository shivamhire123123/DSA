typedef struct {
	int row;
	int col;
}pos_t;
typedef struct node {
	pos_t pos;
	struct node *next;
}node_t;
typedef struct stack {
	node_t *s;
	node_t *buff;
}stack_t;
void init(stack_t *s);
void push(stack_t *s, pos_t pos);
pos_t pop(stack_t *s);
pos_t top(stack_t *s);
int isempty(stack_t *s);
int isfull(stack_t *s);
void print(stack_t *s);
void frees(stack_t *s);
