#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "stack.h"
#define ALLOWED 1
#define NOTALLOWED 0

struct direction {
	int vert;
	int horiz;
};
int main(int argc, char *argv[]) {

	int i, j, path = 0, nextrow, nextcol;
	stack_t s;
	struct direction dir[8] = {
		{.vert = -1, .horiz = 0},
		{.vert = -1, .horiz = 1},
		{.vert = 0, .horiz = 1},
		{.vert = 1, .horiz = 1},
		{.vert = 1, .horiz = 0},
		{.vert = 1, .horiz = -1},
		{.vert = 0, .horiz = -1},
		{.vert = -1, .horiz = -1} };
	pos_t pos = { .row = 1, .col = 1 }, next_pos;
	init(&s);
	if(argc != 2) {
		printf("Please select a maze file");
		return EINVAL;
	}
	FILE *mazefile = fopen(argv[1], "r");
	fscanf(mazefile, "%d%d", &i, &j);
	int rows = i,
	    cols = j;
	int **maze, **visited;
	maze = (int **)malloc(sizeof(int *) * (rows + 2));
	visited = (int **)malloc(sizeof(int *) * (rows + 2));
	for(i = 0; i < rows + 2; i++) {
		maze[i] = (int *)malloc(sizeof(int) * (cols + 2));
		visited[i] = (int *)malloc(sizeof(int) * (cols + 2));
	}
	for(i = 1; i < rows + 1; i++) {
		for(j = 1; j < cols + 1; j++) {
			visited[i][j] = 0;
			fscanf(mazefile, "%d", &maze[i][j]);
			printf("%d ", maze[i][j]);
		}
		printf("\n");
	}
	for(j = 0; j < cols + 2; j++) {
		maze[0][j] = NOTALLOWED;
		maze[rows + 1][j] = NOTALLOWED;
	}
	for(j = 0; j < rows + 2; j++) {
		maze[j][0] = NOTALLOWED;
		maze[j][cols + 1] = NOTALLOWED;
	}
	push(&s, pos);
	while(!isempty(&s)) {
		pos = pop(&s);
		for(i = 0; i < 8; i++) {
			next_pos.row = nextrow = pos.row + dir[i].vert;
			next_pos.col = nextcol = pos.col + dir[i].horiz;
			if(nextrow == rows && nextcol == cols) {
				path = 1;
				push(&s, pos);
				push(&s, next_pos);
				break;
			}
			if(maze[nextrow][nextcol] == ALLOWED &&
				visited[nextrow][nextcol] == 0) {
				visited[nextrow][nextcol] = 1;
				push(&s, next_pos);
			}
		}
		if(path == 1)
			break;
	}
	if(path == 1) {
		print(&s);
	}
	else {
		printf("Path not found\n");
	}
	fclose(mazefile);
	return 0;

}
