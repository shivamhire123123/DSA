#include <stdio.h>
#include <string.h>
#include <errno.h>
#define MAXLEN 128
int lengthof(char str[])
{
	int len;
	for(len = 0;str[len] != '\0';len++);
	return len;
}
char* likestrstr(char *str, char *a) {
	int i = 0, j = 0, flag = 0;
	while(str[i] != '\0') {
		if(str[i] == a[0]) {
			j = 0;
			flag = 1;
			while(a[j] != '\0') {
				if(str[i + j] != a[j]) {
					flag = 0;
					break;
				}
				j++;
			}
			if(flag) {
				return &str[i];
			}
		}
		i++;
	}
	return 0;
}

//
// To shift elements of array starting from 'start' and ending at 'stop' and by 'shift'
// number of elements. The direction of shift is decided by the sign of shift.
// If it is +ve then it is a right shift and left shift if it is -ve.
// Maximum numbers that 'str' can store is pass in maxlen
// return ENOMEM if there is not enough space in 'str' to perform the shift
// return 0 else
//
int shift_arr(char str[], int start, int stop, int shift ,int maxlen)
{
	int index;
	if(shift < 0)
	{
		for(index = start; index <= stop; index++)
			str[index + shift] = str[index];
	}
	else
	{
		if(maxlen <= lengthof(str) + shift)
		{
			return ENOMEM;
		}

		for(index = stop; index >= start; index--)
			str[index + shift] = str[index];
	}
	return 0;
}
//repalces all occureces of 'orig' in 'text' by 'new'
//return number of replaces done
int stringreplace(char *text, char *orig, char *new) {
	int text_lenght,
	    new_length = lengthof(new),
	    orig_length = lengthof(orig),
	    shift = new_length - orig_length,
	    replace_num = 0,
	    i;
	char *ptr;
	ptr = text;
	while((ptr = likestrstr(ptr, orig))) {
		i = 0;
		replace_num++;
		text_lenght = lengthof(text);
		if((shift_arr(text, ptr - text + orig_length, text_lenght, shift, MAXLEN)))
		       	return -1;
		while(i < new_length) {
			ptr[i] = new[i];
			i++;
		}
		ptr = ptr + new_length;
	}
	return replace_num;
}

int main() {
	char text[MAXLEN], orig[MAXLEN], new[MAXLEN];
    	int count;
    	while(scanf("%s%s%s", text, orig, new) != -1) {
       		count = stringreplace(text, orig, new);
       		printf("%d %s\n", count, text);
	}
	return 0;
}
