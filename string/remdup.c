#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#define MAXLEN 100
//
// To shift elements of array starting from 'start' and ending at 'stop' and by 'shift'
// number of elements. The direction of shift is decided by the sign of shift.
// If it is +ve then it is a right shift and left shift if it is -ve.
// 'len' stores the current length of 'str'
// Maximum number of numbers that 'str' can store is pass in maxlen
// return ENOMEM if there is not enough space in 'str' to perform the shift
// return 0 else
//
int shift_arr(double str[], int start, int stop, int shift , int len, int maxlen)
{
	int index;
	if(shift < 0)
	{
		for(index = start;index <= stop;index++)
			str[index + shift] = str[index];
	}
	else
	{
		if(maxlen <= len + shift)
		{
			return ENOMEM;
		}

		for(index = stop;index >= start;index--)
			str[index + shift] = str[index];
	}
	return 0;
}
//delete the 'a'th element from 's' whose size is 'n'
void delete_element(double *s, int a, size_t n) {
	shift_arr(s, a + 1, n - 1, -1, n, MAXLEN);
}
//remove duplicate numbers from 'a' whose size is 'n'
int removeduplicate(double a[], int n) {
	int i = 0,
	    j = 0;
	while(i <  n) {
		j = i + 1;
		while(j < n) {
			if(a[i] == a[j]) {
				delete_element(a, j, n);
				n--;
				j--;
			}
			j++;
		}
		i++;
	}
	return n;
}
int main() {
	int i = 0, j;
	double a[MAXLEN];
	while(i < MAXLEN && scanf("%lf", &a[i]) != -1) {
		i++;
	}
	j = removeduplicate(a, i);
	i = 0;
	while(i < j) {
		printf("%lf ", a[i]);
		i++;
	}
	printf("\n");
	return 0;
}
