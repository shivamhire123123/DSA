#include <stdio.h>
#include <string.h>
#define MAXLINELEN	(100)	/* Max number of character in one line */
#define MAXNUMLINE	(100)	/* Max number of lines */
int main() {
	char data[MAXLINELEN][MAXNUMLINE], c, search_string[MAXNUMLINE];
	int i, j;
	/* Take data which is to searched for search_string */
	for(i = 0; i < MAXNUMLINE; i++) {
		for(j = 0; j < MAXLINELEN && (c = getchar()) != '\n';j++) {
			data[i][j] = c;
		}
		data[i][j] = '\0';
		/* break when no character is inputed */
		if(j == 0) {
			break;
		}
	}
	/* take string to be searched */
	scanf("%s", search_string);
	/*  search for search_string in data */
	for(j = 0; j < i; j++) {
		/* if found print it */
		if(strstr(&data[j][0], search_string) != 0) {
			printf("%s\n", &data[j][0]);
		}
	}
	return 0;
}
