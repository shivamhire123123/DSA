#include <stdio.h>
#include <string.h>
#define MAXLEN	(25)			/* Maximum length of name */
#define MAXSTU 	(80)			/* Maximum number of student */
typedef struct student {
	char name[MAXLEN];
	int age;
	double marks;
}student_t;

int main() {
	student_t student[MAXSTU];
	int i, j;
	char key[MAXLEN];
	for(i = 0; i < MAXSTU; i++) {
		scanf("%s%d%lf", &student[i].name[0], &student[i].age, &student[i].marks);
		if(student[i].age < 0) {
			break;
		}
	}
	scanf("%s", key);
	for(j = 0; j <= i && j < MAXSTU; j++) {
		if(!strcmp(student[j].name, key)) {
			printf("%s %d %lf\n", student[j].name, student[j].age, student[j].marks);
		}
	}
	return 0;
}

