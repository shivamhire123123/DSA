#include "matrices.h"
int main() {
	int matA[MAXSIZE][MAXSIZE], matB[MAXSIZE][MAXSIZE], matC[MAXSIZE][MAXSIZE], m, n, p;
	scanf("%d%d%d", &m, &n, &p);
	getmat(matA, m, n);
	getmat(matB, n, p);
	mulmat(matA, matB, matC, m, n, p);
	printmat(matC, m, p);	
	return 0;
}

