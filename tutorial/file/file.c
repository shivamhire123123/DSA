// General purpose function for file handling
#include "file.h"

int freadline(int fd, char *line, int max) {
	int i = 0, num;
	do {
		num = read(fd, &line[i], sizeof(char));
	} while(num && line[i++] != '\n' && i < max);
	line[i] = '\0';
	return i;
}

int fsize(const char *c) {
	struct stat st;
	stat(c, &st);
	return st.st_size;
}

int countcwl(int fd, int *bc, int *wc,  int *lc) {
	char ch;
	struct stat st;
	flag_t in_word = 0;
	fstat(fd, &st);
	*bc = st.st_size;
	*wc = 0;
	*lc = 0;
	while(read(fd, &ch, sizeof(char))) {
		if(ch == ' ' || ch == '\t') {
			in_word = 0;
		}
		else if(ch == '\n') {
			(*lc)++;
			in_word = 0;
		}
		else if(!in_word) {
			(*wc)++;
			in_word = 1;
		}
	}
	return 0;
}
