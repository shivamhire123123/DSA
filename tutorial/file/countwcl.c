#include "file.h"

int main(int argc, char *argv[]) {
	int fd;
	int bc, wc, lc;
	fd = open(argv[1], O_RDONLY);
	if(fd == -1) {
		perror("");
		return errno;
	}
	countcwl(fd, &bc, &wc, &lc);
	printf("%d %d %d\n", bc, wc, lc);
	return 0;
}
