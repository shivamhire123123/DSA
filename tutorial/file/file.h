// General purpose function for file handling
#ifndef FILE_H
#define FILE_H
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#define MAX 1000

typedef bool flag_t;

/*
 * Read line in file pointed by fd into line array having maximum capacity of
 * max
 * Error condition not checked
 */
int freadline(int fd, char *line, int max);

/*
 * return size of file in bytes
 * doesnt check for errors
 */
int fsize(const char *c);

/*
 * count number of words, bytes and lines in a file
 */
int countcwl(int fd, int *bc, int *wc, int *lc);
#endif
