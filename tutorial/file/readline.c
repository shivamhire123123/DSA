#include "file.h"

int main(int argc, char *argv[]) {
	int fd;
	char line[MAX];
	fd = open(argv[1], O_RDONLY);
	if(fd == -1) {
		perror("");
		return errno;
	}
	while(freadline(fd, line, MAX)) {
		printf("%s", line);
	}
	return 0;
}
